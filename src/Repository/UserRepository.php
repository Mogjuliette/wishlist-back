<?php

namespace App\Repository;

use App\Entities\User;
use PDO;

class UserRepository{
    private PDO $connection;

    public function __construct() {
        $this->connection = Database::connect();
    }

    public function findAll(): array
    {
        /** @var User[] */
        $users = [];


        $statement = $this->connection->prepare('SELECT * FROM user');

        $statement->execute();

        $results = $statement->fetchAll();
        foreach ($results as $item) {
            $users[] = $this->sqlToUser($item);
        }
        return $users
        
        ;
    }

    private function sqlToUser(array $line):User {
        return new User($line['username'], $line['password']);
    }




}