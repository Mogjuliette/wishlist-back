<?php

namespace App\Entities;

class GiftingEvent {

    private ?int $id;

    private string $eventName;
    public function __construct(string $eventName, ?int $id = null) {
    	$this->id = $id;
    	$this->eventName = $eventName;
    }
	/**
	 * @return 
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param  $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getEventName(): string {
		return $this->eventName;
	}
	
	/**
	 * @param string $eventName 
	 * @return self
	 */
	public function setEventName(string $eventName): self {
		$this->eventName = $eventName;
		return $this;
	}
}