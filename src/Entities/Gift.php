<?php

namespace App\Entities ;

class Gift {

    private ?int $id;

    private int $idUserWantingGift;

    private string $name;

    private string $description;

    private string $image;

    private bool $isOffered;
    
    private ?int $idUserOfferingGift;

    public function __construct(int $idUserWantingGift, string $name, string $description, string $image, bool $isOffered, ?int $id = null, ?int $idUserOfferingGift) {
    	$this->id = $id;
    	$this-> idUserWantingGift = $idUserWantingGift;
    	$this-> name = $name;
        $this-> description = $description;
        $this-> image = $image;
        $this-> isOffered = $isOffered;
        $this -> idUserOfferingGift = $idUserOfferingGift;
    }
	/**
	 * @return 
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param  $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getIdUserWantingGift(): int {
		return $this->idUserWantingGift;
	}
	
	/**
	 * @param int $idUserWantingGift 
	 * @return self
	 */
	public function setIdUserWantingGift(int $idUserWantingGift): self {
		$this->idUserWantingGift = $idUserWantingGift;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getName(): string {
		return $this->name;
	}
	
	/**
	 * @param string $name 
	 * @return self
	 */
	public function setName(string $name): self {
		$this->name = $name;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getDescription(): string {
		return $this->description;
	}
	
	/**
	 * @param string $description 
	 * @return self
	 */
	public function setDescription(string $description): self {
		$this->description = $description;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getImage(): string {
		return $this->image;
	}
	
	/**
	 * @param string $image 
	 * @return self
	 */
	public function setImage(string $image): self {
		$this->image = $image;
		return $this;
	}
	
	/**
	 * @return bool
	 */
	public function getIsOffered(): bool {
		return $this->isOffered;
	}
	
	/**
	 * @param bool $isOffered 
	 * @return self
	 */
	public function setIsOffered(bool $isOffered): self {
		$this->isOffered = $isOffered;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getIdUserOfferingGift(): ?int {
		return $this->idUserOfferingGift;
	}
	
	/**
	 * @param  $idUserOfferingGift 
	 * @return self
	 */
	public function setIdUserOfferingGift(?int $idUserOfferingGift): self {
		$this->idUserOfferingGift = $idUserOfferingGift;
		return $this;
	}
}