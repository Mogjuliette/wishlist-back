<?php

namespace App\Entities ;

class Comment {

    private ?int $id ;

    private string $text ;

    private int $idUser ;

    private int $idGift ;

	/**
	 * @return 
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param  $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getText(): string {
		return $this->text;
	}
	
	/**
	 * @param string $text 
	 * @return self
	 */
	public function setText(string $text): self {
		$this->text = $text;
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getIdUser(): int {
		return $this->idUser;
	}
	
	/**
	 * @param int $idUser 
	 * @return self
	 */
	public function setIdUser(int $idUser): self {
		$this->idUser = $idUser;
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getIdGift(): int {
		return $this->idGift;
	}
	
	/**
	 * @param int $idGift 
	 * @return self
	 */
	public function setIdGift(int $idGift): self {
		$this->idGift = $idGift;
		return $this;
	}
}