-- Active: 1674203942125@@127.0.0.1@3306
 DROP DATABASE wishlist ;

CREATE DATABASE wishlist
 DEFAULT CHARACTER SET = 'utf8mb4';

USE wishlist; 


 CREATE TABLE 
    user (
        id INT PRIMARY KEY AUTO_INCREMENT,
        username VARCHAR(255) NOT NULL,
        password VARCHAR(255) NOT NULL
    ); 

 CREATE TABLE
    giftingEvent (
        id INT PRIMARY KEY AUTO_INCREMENT,
        eventName VARCHAR (255)
    ); 

CREATE TABLE
    userEvent (
        id_user INT,
        id_event INT,
        PRIMARY KEY (id_user,id_event),
        FOREIGN KEY (id_user) REFERENCES user(id),
        FOREIGN KEY (id_event) REFERENCES giftingEvent(id) 
    );

CREATE TABLE
    gift (
        id INT PRIMARY KEY AUTO_INCREMENT,
        id_userWantingGift int,
        name VARCHAR(255),
        description VARCHAR(3000),
        image VARCHAR(3000),
        isOffered BOOLEAN,
        id_userOfferingGift int
    );

CREATE TABLE
    comment (
        id INT PRIMARY KEY AUTO_INCREMENT,
        text VARCHAR(500),
        id_user INT,
        id_gift INT,
        isPublic BOOLEAN
    );

INSERT INTO user (username,password) VALUES ("Juliette","123soleil"),("Sophie","456nuage"),("Clément","BGHJKL");

INSERT into giftingEvent (eventName) VALUES ("Noël 2023"),("Noël montagne"),("anniversaire Clément et Alice"),("anniv Loulou");

INSERT into userEvent (id_user,id_event) VALUES (1,1),(1,2),(1,4),(2,1),(2,4),(3,2),(3,3) ;
/* 
SELECT * FROM user; */

SELECT * FROM giftingEvent;